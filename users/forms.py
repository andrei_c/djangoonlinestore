from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput

from users.models import ExtendUser


class UserCreateForm(UserCreationForm):
    class Meta:
        model = ExtendUser
        fields = ['first_name', 'last_name', 'username', 'email', 'phone']
        widgets = {
            'first_name': TextInput(attrs={
                'placeholder': "Please insert first name",
                'class': 'input is-primary'
            }),
            'last_name': TextInput(attrs={
                'placeholder': "Please insert last name",
                'class': 'input is-primary'
            }),
            'username': TextInput(attrs={
                'placeholder': "Please insert desired username",
                'class': 'input is-primary'
            }),
            'email': TextInput(attrs={
                'placeholder': "Please insert your email adress",
                'class': 'input is-primary'
            }),
            'phone': TextInput(attrs={
                'placeholder': "Please insert your phone number",
                'class': 'input is-primary'
            }),
        }

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'input is-primary'
        self.fields['password1'].widget.attrs['placeholder'] = 'Please insert password'
        self.fields['password2'].widget.attrs['class'] = 'input is-primary'
        self.fields['password2'].widget.attrs['placeholder'] = 'Please insert password confirmation'

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data
