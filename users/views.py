from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, permission_required

from django.shortcuts import redirect, render

from users.forms import UserCreateForm


@login_required
@permission_required('auth.add_user', login_url="/login")
def sign_up(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreateForm
    return render(request, 'users/signup.html', {'form': form})
