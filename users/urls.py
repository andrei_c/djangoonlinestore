from django.urls import path

from users import views

urlpatterns= [
    path('users-create/', views.sign_up, name='users_create')
]
