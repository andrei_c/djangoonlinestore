from django.urls import path

from product.views import ProductListView, ProductsListView, ProductCreateView, ProductUpdateView, ProductDeleteView,\
    ProductDetailsView

urlpatterns = [
    path('', ProductListView.as_view(), name='index'),
    path('product-list/', ProductsListView.as_view(), name='product_list'),
    path('product-create/', ProductCreateView.as_view(), name='product_create'),
    path('product-update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),
    path('product-delete/<int:pk>/', ProductDeleteView.as_view(), name='product_delete'),
    path('product-details/<int:pk>/', ProductDetailsView.as_view(), name='product_details'),
]
