from django import forms
from django.forms import TextInput

from product.models import Product


class ProductsCreateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'product_name': TextInput(attrs={
                'placeholder': "Please insert product name",
                'class': 'form-control'
            }),
            'product_description': TextInput(attrs={
                'placeholder': 'Please insert description',
                'class': 'form-control'
            })
        }

    def __init__(self, *args, **kwargs):
        super(ProductsCreateForm, self).__init__(*args, **kwargs)
        self.fields['product_name'].required = True
        self.fields['product_description'].required = False

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('product_name')
        all_products = Product.objects.all()
        for product in all_products:
            if name == product.product_name:
                msg = 'Product is already in the database'
                self._errors['product_name'] = self.error_class([msg])
        return cleaned_data
