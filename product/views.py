from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from product.forms import ProductsCreateForm
from product.models import Product


class ProductListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    # view product - details
    template_name = 'product/product-details.html'
    model = Product
    permission_required = 'product.view_product'


class ProductsListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    # view products in category
    template_name = 'product/product-list.html'
    model = Product
    context_object_name = 'all_products'
    permission_required = 'product.view_product'


class ProductCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    # create new product
    template_name = 'product/product-create.html'
    model = Product
    form_class=ProductsCreateForm
    # fields = '__all__'
    # fields = [
    #     'product_name',
    #     'product_sku',
    #     'product_category',
    #     'product_price',
    #     'product_description'
    # ]
    success_url = reverse_lazy('product_list')
    permission_required = 'product.add_product'


class ProductUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    # edit product details
    template_name = 'product/product-update.html'
    model = Product
    fields = '__all__'
    # fields = [
    #     'product_name',
    #     'product_sku',
    #     'product_category',
    #     'product_price',
    #     'product_description'
    # ]
    success_url = reverse_lazy('product_list')
    permission_required = 'product.change_product'


class ProductDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    # delete product
    template_name = 'product/product-delete.html'
    model = Product
    fields = '__all__'
    success_url = reverse_lazy('product_list')
    permission_required = 'product.delete_product'


class ProductDetailsView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'product/product-details.html'
    model = Product
    context_object_name = 'product_details'
    success_url = reverse_lazy('product_list')
    permission_required = 'product.view_product'
