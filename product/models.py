from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=30)
    product_sku = models.CharField(max_length=5)
    product_price = models.IntegerField()
    product_description = models.CharField(max_length=300)
    category = models.ForeignKey('category.Category', on_delete=models.CASCADE, default=None, null=False)

    def __str__(self):
        return self.product_name
