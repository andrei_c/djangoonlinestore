from django.urls import path

from category.views import CategoryListView, CategoryCreateView, CategoryUpdateView, CategoryDeleteView,\
    CategoryDetailsView


urlpatterns = [
    path('', CategoryListView.as_view(), name='index'),
    path('category-list/', CategoryListView.as_view(), name='category_list'),
    path('category-create/', CategoryCreateView.as_view(), name='category_create'),
    path('category-update/<int:pk>/', CategoryUpdateView.as_view(), name='category_update'),
    path('category-delete/<int:pk>/', CategoryDeleteView.as_view(), name='category_delete'),
    path('category-details/<int:pk>/', CategoryDetailsView.as_view(), name='category_details'),
]
