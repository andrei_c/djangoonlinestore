from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView


from category.models import Category

from category.forms import CategoryCreateForm


class CategoryListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    # view products in category
    template_name = 'category/category-list.html'
    model = Category
    context_object_name = 'all_categories'
    permission_required = 'category.view_category'


class CategoryCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    # create new product
    template_name = 'category/category-create.html'
    model = Category
    form_class = CategoryCreateForm
    success_url = reverse_lazy('category_list')
    permission_required = 'category.add_category'


class CategoryUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    # edit product details
    template_name = 'category/category-update.html'
    model = Category
    fields = [
        'category_name',
        'category_description'
    ]
    success_url = reverse_lazy('category_list')
    permission_required = 'category.change_category'


class CategoryDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    # delete product
    template_name = 'category/category-delete.html'
    model = Category
    fields = '__all__'
    success_url = reverse_lazy('category_list')
    permission_required = 'category.delete_category'


class CategoryDetailsView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'category/category-details.html'
    model = Category
    context_object_name = 'category_details'
    success_url = reverse_lazy('category_list')
    permission_required = 'category.view_category'
