from django import forms
from django.forms import TextInput

from category.models import Category


class CategoryCreateForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            'category_name',
            'category_description'
        ]
        widgets = {
            'category_name': TextInput(attrs={
                'placeholder': 'Please insert category name',
                'class': 'form-control'
            }),
            'category_description': TextInput(attrs={
                'placeholder': 'Please insert description',
                'class': 'form-control'
            })
        }

    def __init__(self, *args, **kwargs):
        super(CategoryCreateForm, self).__init__(*args, **kwargs)
        self.fields['category_name'].required = True
        self.fields['category_description'].required = False

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('category_name')
        #description = cleaned_data.get('category_description')
        all_categories = Category.objects.all()
        for category in all_categories:
            if name == category.category_name:
                msg = 'Category is already in the database'
                self._errors['category_name'] = self.error_class([msg])
        return cleaned_data
